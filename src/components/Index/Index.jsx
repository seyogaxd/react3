import Header from "./Header/Header"
import Main from "./Main/Main"
import PropTypes from "prop-types";
import './Styles/IndexStyles.scss'
import { useState, useEffect } from "react";
import ModalText from "../Modal/ModalTypes/ModalText"

export default function IndexPage(props)
{   
    const {
        items,
        ShopItems,
        FavoriteItems,
        openModal,
        closeModal,
        addToShoped,
        addToFavorite,
        isModalOpen,
        title,
        product,
    } = props;
    
    return(
        <>
        <Main
        title={title}
        modalOpen={openModal}
        addToFavorite={addToFavorite}
        itemslist={items}
        favoriteMassive={FavoriteItems}
        shopedMassive={ShopItems}
        />
         {isModalOpen&&(
            <ModalText
            title={`Buy Product "${product.name}"`}
            text={`color: ${product.color} cost: ${product.price} article: ${product.article}`}
            closeWindow={closeModal}
            onClick={addToShoped}
            />
         )

         }   
        </>
    )
}