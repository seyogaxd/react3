import FavoriteIcon from "../../../../assets/icons/favorite-icon.svg"
import FavoriteIconPressed from "../../../../assets/icons/favorite-icon-pressed.svg"
import TestIcon from "../../../../../public/images/red-tshirt.png"
import Button from "../../../Button/Button"

export default function ShopItem(props)
{
    const {
        modalOpen,
        imageModalOpen,
        id,
        addToFavorite,
        image,
        name,
        price,
        isFavorite,
        isBoughted
    } = props;


    return(
        <div className="main__item">
            <div className="main__item-title">
            <p className="main__item-name">{name}</p>
            {imageModalOpen&&(
                <span className="main__item-delete">
                <Button 
                classNames={"main__item-delete"}
                onClick={() => imageModalOpen(id)}
                >
                    &times;
                </Button>
                </span>
            )}
            
            </div>
            <div className="main__item-image"><img src={image} alt="TestIcon" /></div>
            <div className="main__item-buttons">
            {!isFavorite&&(
                <Button
            type={'button'}
            classNames={'main__item-favorite-button'}
            onClick={addToFavorite}
            >
                <img src={FavoriteIcon} alt="Favorite-icon" />
            </Button>
            )}
            {isFavorite&&(
                <Button
                type={'button'}
                classNames={'main__item-favorite-button'}
                onClick={addToFavorite}
                >
                    <img src={FavoriteIconPressed} alt="Favorite-icon" />
                </Button>
            )}
            <span className="main__item-price">{price}</span>
            {!isBoughted&&(
                <Button
                type={'button'} 
                classNames={'main__item-button'} 
                onClick={() => modalOpen(id)}
            >
            Buy
            </Button>
            )}
            {isBoughted&&(
                <div className="main__item-button-boughted">Purchased</div>
            )
            }
            </div>
        </div>
    )
}
