import ShopItem from "../ShopItem/ShopItem";

export default function ShopList(props)
{
    const {
        modalOpen,
        imageModalOpen,
        itemslist,
        addToShoped,
        addToFavorite,
        favoriteMassive,
        shopedMassive
    } = props;
    return(
                itemslist.map((item) => <ShopItem 
                key={item.id} 
                id={item.id}
                modalOpen={modalOpen} 
                imageModalOpen={imageModalOpen}
                addToFavorite={() => addToFavorite(item)} 
                image={item.image} 
                addToShoped={addToShoped}
                name={item.name}
                price={item.price}
                isFavorite={favoriteMassive.some((thisItem) => thisItem.id === item.id)}
                isBoughted={shopedMassive.some((thisItem) => thisItem.id === item.id)}
                />)
    )
}