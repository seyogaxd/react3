import ModalHeader from "../ModalHeader/ModalHeader";
import ModalBody from "../ModalBody/ModalBody";
import ModalClose from "../ModalClose/ModalClose";
import ModalFooter from "../ModalFooter/ModalFooter";
import ModalWrapper from "../ModalWrapper/ModalWrapper";
import Modal from "../Modal";

export default function ModalImage({ className, title, text, closeWindow, onClick, imageSource, imageName })
{
    return(
        <ModalWrapper closeWindow={closeWindow}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={closeWindow}></ModalClose>
                </ModalHeader>
                <ModalBody>
                    <img src={imageSource} alt={imageName ? imageName : 'picture'} className={className}/>
                    <h2>{title}</h2>
                    <p>{text}</p>
                </ModalBody>
                <ModalFooter
                firstText={"NO, CANCEL"}
                secondaryText={"YES, DELETE"}
                firstClick={closeWindow}
                secondaryClick={onClick}
                >
                </ModalFooter>
            </Modal>
        </ModalWrapper>

    )
}