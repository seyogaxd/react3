
import ShopItem from "../../Index/Main/ShopItem/ShopItem";
import BasketMain from "../Basket/Main/BasketMain"
import ModalImage from "../../Modal/ModalTypes/ModalImage"

export default function BasketPage(props){
    const {
        FavoriteItems,
        addToShoped,
        addToFavorite,
        favoriteMassive,
        imageModalOpen,
        shopedMassive,
        modalOpen,
        title,
        isimageModalOpen,
        closeImageModal,
        deletingItem,
        product,
    } = props;
    return(
        <>
            <BasketMain
            FavoriteItems={FavoriteItems}
            addToShoped={addToShoped}
            addToFavorite={addToFavorite}
            favoriteMassive={favoriteMassive}
            imageModalOpen={imageModalOpen}
            shopedMassive={shopedMassive}
            modalOpen={modalOpen}
            title={title}
            >
            </BasketMain>
            {isimageModalOpen&&(
                 <ModalImage
                 className={'main__modal-image'}
                 imageSource={product.image}
                 imageName={product.name+".png"}
                 title={'Product Delete!'}
                 text={`By clicking the “Yes, Delete” button, "${product.name}" will be deleted.`}
                 closeWindow={closeImageModal}
                 onClick={() => deletingItem(product.id)}
                 />
            )}
        </>
        
    )
}