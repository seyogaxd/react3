
import ShopItem from "../../../Index/Main/ShopItem/ShopItem";

export default function BasketPage(props){
    const {
        FavoriteItems,
        addToShoped,
        addToFavorite,
        favoriteMassive,
        imageModalOpen,
        shopedMassive,
        modalOpen,
        title
    } = props;

    return(
        <div className="main__container">
            <h1>{title}</h1>
            <div className="main__container-items">
                {shopedMassive.length === 0 ? (
                    <h2>You have no boughted products yet</h2>
                ) : (
                    <div className="main__items-list">
                        {shopedMassive.map((item) => (
                            <ShopItem 
                                key={item.id} 
                                id={item.id}
                                modalOpen={modalOpen} 
                                addToFavorite={() => addToFavorite(item)} 
                                image={item.image} 
                                addToShoped={addToShoped}
                                name={item.name}
                                price={item.price}
                                imageModalOpen={imageModalOpen}
                                isFavorite={favoriteMassive.some((thisItem) => thisItem.id === item.id)}
                                isBoughted={shopedMassive.some((thisItem) => thisItem.id === item.id)}
                            />
                        ))}
                    </div>
                )}
            </div>
        </div>
    )
}
