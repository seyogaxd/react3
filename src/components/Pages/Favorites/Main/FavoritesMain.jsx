
import ShopItem from "../../../Index/Main/ShopItem/ShopItem";

export default function FavoriteMain(props){
    const {
        FavoriteItems,
        addToShoped,
        addToFavorite,
        favoriteMassive,
        shopedMassive,
        modalOpen,
        title
    } = props;

    return(
        <div className="main__container">
            <h1>{title}</h1>
            <div className="main__container-items">
                {favoriteMassive.length === 0 ? (
                    <h2>You have no favorite products yet</h2>
                ) : (
                    <div className="main__items-list">
                        {favoriteMassive.map((item) => (
                            <ShopItem 
                                key={item.id} 
                                id={item.id}
                                modalOpen={modalOpen} 
                                addToFavorite={() => addToFavorite(item)} 
                                image={item.image} 
                                addToShoped={addToShoped}
                                name={item.name}
                                price={item.price}
                                isFavorite={favoriteMassive.some((thisItem) => thisItem.id === item.id)}
                                isBoughted={shopedMassive.some((thisItem) => thisItem.id === item.id)}
                            />
                        ))}
                    </div>
                )}
            </div>
        </div>
    )
}