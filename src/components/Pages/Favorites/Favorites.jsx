
import FavoriteMain from "./Main/FavoritesMain";
import ModalText from "../../Modal/ModalTypes/ModalText";

export default function FavoritePage(props){
    const {
        FavoriteItems,
        addToShoped,
        addToFavorite,
        favoriteMassive,
        shopedMassive,
        modalOpen,
        title,
        isModalOpen,
        closeModal,
        product
    } = props;
    return(
        <>
        <FavoriteMain
        FavoriteItems={FavoriteItems}
        addToShoped={addToShoped}
        addToFavorite={addToFavorite}
        favoriteMassive={favoriteMassive}
        shopedMassive={shopedMassive}
        modalOpen={modalOpen}
        title={title}
        >
        </FavoriteMain>
        {isModalOpen&&(
            <ModalText
            title={`Buy Product "${product.name}"`}
            text={`color: ${product.color} cost: ${product.price} article: ${product.article}`}
            closeWindow={closeModal}
            onClick={addToShoped}
            />
         )

         }   
        </>
    )
}