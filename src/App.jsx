import { useState, useEffect } from "react";

import './App.css'

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { NavLink } from "react-router-dom";


import ModalText from "./components/Modal/ModalTypes/ModalText";
import Header from "./components/Index/Header/Header";

import Index from './components/Index/Index'
import Favorites from './components/Pages/Favorites/Favorites'
import BasketPage from './components/Pages/Basket/Basket';


function App() {
  const [items, setItems] = useState([]);

    const sendRequest = async (url) => {
        const response = await fetch(url);
        const items = await response.json();
        return items;
    }
        

    useEffect(() => {
        sendRequest("items.json").then((items) => {
            setItems(items);
        });
    }, 
    []
    );



    const [ShopItems, setShopItems] = useState(
        JSON.parse(localStorage.getItem("ShopItems")) || []
    );


    const [FavoriteItems, setFavoriteItems] = useState(
        JSON.parse(localStorage.getItem("FavoriteItems")) || []
    );


    //console.log(FavoriteItems);

    //localStorage.clear();

    const [isModalOpen, setModalOpen] = useState(false)
    const [isImageModalOpen, setImageOpen] = useState(false)
      
    const [modalItemId, setModalItemId] = useState(null);

    const openModal = (itemId) => {
        setModalItemId(itemId);
        setModalOpen(true);
    };
    const openImageModal=(itemId)=>
    {
        setModalItemId(itemId);
        setImageOpen(true);
    }
    // const openImageModal=(itemId)=>{
    //     setModalItemId(itemId);
    //     setImageOpen(true)
    // }
      
    
    const closeModal=()=>setModalOpen(false)
    const closeImageModal=()=>setImageOpen(false)

   // const addToFavorite=(item)=>{
   //     setFavoriteItems()
   // }
    const addToShoped = (itemToAdd) => {
        itemToAdd = items[modalItemId-1];
    
        const newItem = {
            id: itemToAdd.id,
            name: itemToAdd.name,
            price: itemToAdd.price,
            image: itemToAdd.image,
            article: itemToAdd.article,
        };
        const newShopedItems = [...ShopItems, newItem];

        setShopItems(newShopedItems);
        localStorage.setItem(
            "ShopItems",
            JSON.stringify(newShopedItems)
        );
        closeModal();
       
};

    const deleteFromShoped = (itemToDelete) => {

        const newShopedItems = ShopItems.filter(
            (item) =>item.id !== itemToDelete
        )

        setShopItems(newShopedItems);
        localStorage.setItem(
            "ShopItems",
            JSON.stringify(newShopedItems)
        );
        closeImageModal();
    }

    const addToFavorite = (itemToAdd) => {
        const itemIdx = FavoriteItems.findIndex(
            (item) => item.id === itemToAdd.id
        );
  
        if (itemIdx === -1) {
            const newItem = {
                id: itemToAdd.id,
                name: itemToAdd.name,
                price: itemToAdd.price,
                image: itemToAdd.image,
                article: itemToAdd.article,
                color: itemToAdd.color,
            };
            
            const newFavoriteItems = [...FavoriteItems, newItem];
  
            setFavoriteItems(newFavoriteItems);

            localStorage.setItem(
                "FavoriteItems",
                JSON.stringify(newFavoriteItems)
            );
           
        } 
        else {
            const newFavoriteItems = FavoriteItems.filter(
                (item) => item.id !== itemToAdd.id
            );
            
            setFavoriteItems(newFavoriteItems);

            localStorage.setItem(
                "FavoriteItems",
                JSON.stringify(newFavoriteItems)
            );
        }
    };

  return (
    <>
        
    <Router>
    <Header
        boughtItemsAmount={ShopItems.length}
        favoriteItemsAmount={FavoriteItems.length}
        />
        <Routes>
        <Route path="/" element={
        <Index
        items={items}
        ShopItems={ShopItems}
        FavoriteItems={FavoriteItems}
        openModal={openModal}
        addToFavorite={addToFavorite}
        closeModal={closeModal}
        addToShoped={addToShoped}
        isModalOpen={isModalOpen}
        title={"Our Products"}
        product={modalItemId ? items[modalItemId-1] : null}
        //productName={modalItemId ? items[modalItemId-1]?.name : null}
        ></Index>
        } />
        <Route path="/favorites" element={
        <Favorites 
        addToShoped={addToShoped}
        addToFavorite={addToFavorite}
        modalOpen={openModal}
        favoriteMassive={FavoriteItems}
        shopedMassive={ShopItems}
        isModalOpen={isModalOpen}
        closeModal={closeModal}
        product={modalItemId ? items[modalItemId-1] : null}
        title={"Your Favorites"}
        />} />
        <Route path="/basket" element={
        <BasketPage
        addToShoped={addToShoped}
        addToFavorite={addToFavorite}
        modalOpen={openModal}
        favoriteMassive={FavoriteItems}
        shopedMassive={ShopItems}
        title={"Your Purchases"}
        imageModalOpen={openImageModal}
        isimageModalOpen={isImageModalOpen}
        closeImageModal={closeImageModal}
        product={modalItemId ? items[modalItemId-1] : null}
        deletingItem={deleteFromShoped}
        />}/>
      </Routes>
    </Router> */
    </>
  );
}

export default App
